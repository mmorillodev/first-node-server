const server = require('./server');
const port = process.env.port || 80;
const courses = [
    { id: 1, name: 'course 1', active: true },
    { id: 2, name: 'course 2', active: true },
    { id: 3, name: 'course 3', active: true },
    { id: 4, name: 'course 4', active: true },
    { id: 5, name: 'course 5', active: true },
];

server.on('GET', (req, res) => {    
    const activeOnly = req.query.activeOnly && req.query.activeOnly == 'true';
    if(req.path === '/api/courses') {
        const filteredCourses = activeOnly ? courses.filter(e => e.active) : courses;
        
        res.end(JSON.stringify({
            length: filteredCourses.length,
            courses: filteredCourses
        }));
    } else {
        const matches = req.path.match(/\/api\/courses\/([0-9]+)/);
        if(matches) {
            const course = courses.find(e => activeOnly ? e.id === parseInt(matches[1]) && e.active : e.id === parseInt(matches[1]));
            if(course)
                res.end(JSON.stringify(course));
            else {
                res.end(JSON.stringify({
                    response: 'error',
                    error: 'The course you queried does not exist' + (activeOnly ? ' or is inactive' : '')
                }));
            }
        } else {
            res.statusCode = 404;
            res.end(JSON.stringify({
                response: 'error',
                error: 'Please, rovide a valid ID'
            }));
        }
    }
}).on('POST', (req, res) => {
    if(req.path === '/api/courses') {     
        if(req.body.name && req.body.name.length > 3) {
            const course = { id: courses.length, name: req.body.name, active: true };
            courses.push(course);

            res.statusCode = 201;
            res.end(JSON.stringify(course));
        } else {
            res.statusCode = 400;
            res.end();
        }
    } else {
        res.statusCode = 404;
        res.end();
    }
}).on('PUT', (req, res) => {
    const matches = req.path.match(/\/api\/courses\/([0-9]+)/);
    if(matches) {
        const course = courses.find(e => e.id === parseInt(matches[1]));
        if(course) {
            if(req.body.name && req.body.name.length > 3) {
                course.name = req.body.name;
                res.end(JSON.stringify(course));
            } else {
                res.statusCode = 400;
                res.end(JSON.stringify({
                    response: 'error',
                    error: 'The name is required and must be 3 characters in minimum'

                }));
            }
        } else {
            res.statusCode = 404;
            res.end(JSON.stringify({
                response: 'error',
                error: `The course with ID ${matches[1]} either does not exist or is inactive`
            }));
        }
    } else {
        res.statusCode = 400;
        res.end(JSON.stringify({
            response: 'error',
            error: 'Please provide the ID of the course to be updated'
        }));
    }
}).on('DELETE', (req, res) => {
    const matches = req.path.match(/\/api\/courses\/([0-9]+)/);
    if(matches) {
        const course = courses.find(e => e.id === parseInt(matches[1]) && e.active);
        if(course) {
            course.active = false;
            res.end(JSON.stringify(course));
        } else {
            res.end(JSON.stringify({
                response: 'error',
                error: `The course with ID ${matches[1]} either does not exist or is inactive`
            }));
        }
    } else {
        res.statusCode = 400;
        res.end();
    }
}).on('OTHER', (req, res) => {
    res.statusCode = 405;
    res.end();
}).listen(port);