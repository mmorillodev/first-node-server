const http = require('http');
const url = require('url');
const EventEmitter = require('events').EventEmitter;

class Server extends EventEmitter {

    listen(port) {
        http.createServer((req, res) => {   
            const decodedUrl = url.parse(req.url, true);
            const req_ = {
                host: decodedUrl.host,
                completeUrl: req.url,
                path: decodedUrl.pathname,
                method: req.method,
                query: decodedUrl.query,
                search: decodedUrl.search,
            };

            res.setHeader('Content-Type', 'application/json');

            if(req.method === 'GET') {
                super.emit('GET', req_, res);
            } else if(req.method === 'POST') {                
                let body = '';
                req.on('data', chunk => body += chunk.toString());
                req.on('end', () => {     
                    req_.body = req.headers['content-type'] == 'application/json' ? JSON.parse(body) : body;
                    super.emit('POST', req_, res);
                });
            } else if(req.method === 'PUT') {
                let body = '';
                req.on('data', chunk => body += chunk.toString());
                req.on('end', () => {
                    req_.body = req.headers['content-type'] == 'application/json' ? JSON.parse(body) : body;
                    super.emit('PUT', req_, res);
                });
            } else if(req.method === 'DELETE') {                
                super.emit('DELETE', req_, res);
            } else {
                super.emit('OTHER', req_, res);
            }
        }).listen(port);
    }
}

let server = new Server();
module.exports = { 
    listen: server.listen,
    on: server.on
};